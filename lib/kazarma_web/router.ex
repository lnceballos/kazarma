# SPDX-FileCopyrightText: 2020  Pierre de Lacroix and others
# SPDX-License-Identifier: AGPL-3.0-only

defmodule KazarmaWeb.Router do
  use KazarmaWeb, :router

  use MatrixAppServiceWeb.Routes
  use ActivityPubWeb.Router

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  # scope "/api", KazarmaWeb do
  #   pipe_through :api
  # end

  MatrixAppServiceWeb.Routes.routes(Application.get_env(:matrix_app_service, :app_service))

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: KazarmaWeb.Telemetry
    end
  end
end
