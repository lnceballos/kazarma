# SPDX-FileCopyrightText: 2020  Pierre de Lacroix and others
# SPDX-License-Identifier: AGPL-3.0-only
defmodule Kazarma.Config do
  defmacro __using__(_) do
    quote do
      @matrix_client Application.get_env(:kazarma, :matrix) |> Keyword.fetch!(:client)
    end
  end
end
