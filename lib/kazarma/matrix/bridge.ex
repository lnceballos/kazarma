# SPDX-FileCopyrightText: 2020  Pierre de Lacroix and others
# SPDX-License-Identifier: AGPL-3.0-only
defmodule Kazarma.Matrix.Bridge do
  @moduledoc """
  Functions for the bridge database.
  """
  use MatrixAppService.BridgeConfig, repo: Kazarma.Repo
end
