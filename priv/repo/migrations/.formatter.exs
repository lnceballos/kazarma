# SPDX-FileCopyrightText: 2020  Pierre de Lacroix and others
# SPDX-License-Identifier: AGPL-3.0-only
[
  import_deps: [:ecto_sql],
  inputs: ["*.exs"]
]
