# SPDX-FileCopyrightText: 2020  Pierre de Lacroix and others
# SPDX-License-Identifier: AGPL-3.0-only
Mox.defmock(Kazarma.Matrix.TestClient, for: MatrixAppService.ClientBehaviour)
